package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/partial-file-upload", PartialFileUploadHandler)
	http.ListenAndServe(":8080", nil)
}

var FILENAME = "./file.jpeg"

func PartialFileUploadHandler(w http.ResponseWriter, r *http.Request) {
	file, _, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintf(w, "error : %s\n", err.Error())
	}

	nBytes, nChunks := int64(0), int64(0)
	buffReader := bufio.NewReader(file)
	buf := make([]byte, 0, 1000)

	f, err := os.OpenFile(FILENAME, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	for {
		n, err := buffReader.Read(buf[:cap(buf)])
		buf = buf[:n]
		if n == 0 {
			if err == nil {
				continue
			}
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}

		if _, err = f.WriteString(string(buf[:])); err != nil {
			panic(err)
		}

		nChunks++
		nBytes += int64(len(buf))
		// process buf
		if err != nil && err != io.EOF {
			log.Fatal(err)
		}
	}

	log.Println("File Upload Bytes:", nBytes, "Chunks:", nChunks)
	fmt.Fprintf(w, "File Upload Bytes: %d Chunks: %d", nBytes, nChunks)
}
